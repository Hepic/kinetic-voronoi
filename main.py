import pygame, sys, datetime
import matplotlib.pyplot as plt
from helpFunctions import readFile
from helpFunctions import SCREEN_HEIGHT, SCREEN_WIDTH, FPS
from ship import Ship
from operations import voronoiOperations, delaunayOperations


def main():
    choice = input('1)Smooth graphics(Voronoi), 2)Efficient search(Delaunay) -> ')
    
    coords, timeSegs, vecSegs, e = readFile(sys.argv[1])
    ships, N = [], len(coords)
    
    # Initialize information of ships
    for i in range(N):
        ships.append(Ship(coords[i], timeSegs[i], vecSegs[i]))

    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption('Kinetic Voronoi')

    running = True 
    clock = pygame.time.Clock()
    counter, minute = 0, 0
    fourCircle, smallDists, allDists = [], [], []

    while running:
        screen.fill((0, 102, 204)) # clear the screen every time
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                running = False
                return
        
        points = [
            (-10 * SCREEN_WIDTH, -10 * SCREEN_HEIGHT), (10 * SCREEN_WIDTH, -10 * SCREEN_HEIGHT),
            (10 * SCREEN_WIDTH, 10 * SCREEN_HEIGHT), (-10 * SCREEN_WIDTH, 10 * SCREEN_HEIGHT)
        ]
        
        # draw the ships
        for i in range(N): 
            shipPos = ships[i].getPos(minute)
            pygame.draw.circle(screen, (255, 255, 255), shipPos, 5, 0)
            points.append(shipPos)

        bef = datetime.datetime.now().replace()
        
        if choice == 1:
            voronoiOperations(pygame, screen, points, e)
        elif choice == 2:
            delaunayOperations(pygame, screen, points, e, fourCircle, smallDists, allDists)
        
        aft = datetime.datetime.now().replace()
        '''
        print aft - bef # time per frame 
        '''
        # count the minutes
        counter += 1
        
        if counter % FPS == 0:
            minute += 1
        
        pygame.display.flip()
        clock.tick(FPS)


if __name__ == '__main__':
    main()
