class Ship:
    def __init__(self, coord, timeSeg, vecSeg):
        self.coord = coord
        self.timeSeg = timeSeg
        self.vecSeg = vecSeg
        self.pos = 0

    def getPos(self, minute):
        if minute >= self.timeSeg[self.pos] and self.pos < len(self.timeSeg) - 1:
            self.pos += 1
        
        # update coordinates relative to vector
        temp = list(self.coord)
        temp[0] += self.vecSeg[self.pos][0]
        temp[1] -= self.vecSeg[self.pos][1]
        
        self.coord = tuple(temp)
        return self.coord
