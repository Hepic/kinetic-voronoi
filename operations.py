from scipy.spatial import Voronoi, Delaunay
from helpFunctions import readFile, euclideanDistance, inCircle


def voronoiOperations(pygame, screen, points, e):
    vor = Voronoi(points)
    
    # draw the voronoi
    for elem in vor.ridge_vertices:
        p1, p2 = elem
        
        if p1 == -1 or p2 == -1:
            continue

        pygame.draw.line(screen, (255, 255, 255), tuple(vor.vertices[p1]), tuple(vor.vertices[p2]))
    
    # check for distances between ships
    for elem in vor.ridge_points:
        p1, p2 = elem
        dist = euclideanDistance(vor.points[p1], vor.points[p2])

        if dist <= e:
            pygame.draw.line(screen, (255, 255, 100), tuple(vor.points[p1]), tuple(vor.points[p2]))


def delaunayOperations(pygame, screen, points, e, fourCircle, smallDists, allDists):
    # counter keeps the current time
    # prevChange keeps the last time since we updated smallDists
    try:
        delaunayOperations.counter += 1
    except AttributeError:
        delaunayOperations.counter = 0
        delaunayOperations.prevChange = 0

    for elem in fourCircle:
        p1, p2, p3, p4 = elem
        
        # check if delaunay triangulation is valid
        if inCircle(points[p1], points[p2], points[p3], points[p4]):
            del fourCircle[:] 
    
    # draw the delaunay
    if not fourCircle:
        delau = Delaunay(points[4:])
        del smallDists[:]
        del allDists[:]

        for i in range(len(delau.simplices)):
            p1, p2, p3 = delau.simplices[i]
            pygame.draw.polygon(screen, (255, 200, 50), [tuple(delau.points[p1]), tuple(delau.points[p2]), tuple(delau.points[p3])], 1)
            
            # Add points to the smallDists, which are relatively close
            dist1 = euclideanDistance(points[p1], points[p2])
            dist2 = euclideanDistance(points[p1], points[p3])
            dist3 = euclideanDistance(points[p2], points[p3])   

            if dist1 <= 2 * e:
                smallDists.append((4 + p1, 4 + p2))
            
            if dist2 <= 2 * e:
                smallDists.append((4 + p1, 4 + p3))

            if dist3 <= 2 * e:
                smallDists.append((4 + p2, 4 + p3))
            
            allDists.extend([(4 + p1, 4 + p2), (4 + p1, 4 + p3), (4 + p2, 4 + p3)])
            delaunayOperations.prevChange = delaunayOperations.counter
                
            # for each triangle find the opposite vertices
            for j in range(len(delau.neighbors[i])):
                neighInd = delau.neighbors[i][j]

                if neighInd != -1:
                    neigh = delau.simplices[neighInd]
                    opposOut = list(set(neigh).difference(set(delau.simplices[i])))[0]
                    
                    # these points will be checked for validation of triangulation
                    fourCircle.append((p1 + 4, p2 + 4, p3 + 4, opposOut + 4))
    
    # update smallDists every five frames
    if delaunayOperations.prevChange + 5 < delaunayOperations.counter:
        del smallDists[:]

        for elem in allDists:
            p1, p2 = elem
            dist = euclideanDistance(points[p1], points[p2])
            
            if dist <= e:
                pygame.draw.line(screen, (255, 255, 100), tuple(points[p1]), tuple(points[p2]))

            if dist <= 2 * e:
                smallDists.append((p1, p2))
            
            delaunayOperations.prevChange = delaunayOperations.counter
    else:
        # check for distances between ships
        for elem in smallDists:
            p1, p2 = elem
            dist = euclideanDistance(points[p1], points[p2])

            if dist <= e:
                pygame.draw.line(screen, (255, 255, 100), tuple(points[p1]), tuple(points[p2]))
