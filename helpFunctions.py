import math

SCREEN_HEIGHT, SCREEN_WIDTH = 900, 1440
FPS = 20


def nextNumber(pos, line):
    num, minus = 0, False
    
    while not line[pos].isdigit():
        pos += 1
    
    if line[pos - 1] == '-':
        minus = True

    while line[pos].isdigit():
        num = num * 10 + int(line[pos])
        pos += 1
    
    if minus:
        num = -num

    return num, pos


def readFile(fileName):
    coords, timeSeg, vecSeg, e = [], [], [], 0

    with open(fileName) as fi:
        for line in fi: 
            pos = 0
            
            if line[0] == 'e':
                e, pos = nextNumber(pos, line)
                break

            x, pos = nextNumber(pos, line)
            y, pos = nextNumber(pos, line)
            coords.append((x, y))
            
            pos += 1
            temp = []
            
            while line[pos] != ')':
                sec, pos = nextNumber(pos, line)
                temp.append(sec)
            
            timeSeg.append(temp)

            pos += 1
            temp = []
            
            while line[pos] != ')':
                x, pos = nextNumber(pos, line)
                y, pos = nextNumber(pos, line)
                pos += 1
                temp.append((x, y))
            
            vecSeg.append(temp)

    return coords, timeSeg, vecSeg, e


def euclideanDistance(pnt1, pnt2):
    dx, dy = pnt2[0] - pnt1[0], pnt2[1] - pnt1[1]
    dist = math.sqrt(dx * dx + dy * dy)

    return dist


def determ(pnt1, pnt2):
    ret = pnt1[0] * pnt2[1] - pnt2[0] * pnt1[1]
    return ret


def inCircle(pnt1, pnt2, pnt3, pnt4):
    dax, dbx, dcx = pnt1[0] - pnt4[0], pnt2[0] - pnt4[0], pnt3[0] - pnt4[0]
    day, dby, dcy = pnt1[1] - pnt4[1], pnt2[1] - pnt4[1], pnt3[1] - pnt4[1]
    
    ret = dax * determ((dby, dcy), (dbx * dbx + dby * dby, dcx * dcx + dcy * dcy))
    ret -= day * determ((dbx, dcx), (dbx * dbx + dby * dby, dcx * dcx + dcy * dcy))
    ret += (dax * dax + day * day) * determ((dbx, dcx), (dby, dcy))

    return ret >= 0
